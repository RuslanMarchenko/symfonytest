<?php

/* base.html.twig */
class __TwigTemplate_c3923f894e5e514019bb50e1f89387c75ee7c150cebfbf4f9e517c76e496b97e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_812bd0d1da35c05c9965bc31550ef969def966d6d0d65e65214e27138fbc6a9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_812bd0d1da35c05c9965bc31550ef969def966d6d0d65e65214e27138fbc6a9f->enter($__internal_812bd0d1da35c05c9965bc31550ef969def966d6d0d65e65214e27138fbc6a9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_4f467ed1db87d4bf155e4510290b7985554e811f327777545c19cd6fa4c297ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f467ed1db87d4bf155e4510290b7985554e811f327777545c19cd6fa4c297ee->enter($__internal_4f467ed1db87d4bf155e4510290b7985554e811f327777545c19cd6fa4c297ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_812bd0d1da35c05c9965bc31550ef969def966d6d0d65e65214e27138fbc6a9f->leave($__internal_812bd0d1da35c05c9965bc31550ef969def966d6d0d65e65214e27138fbc6a9f_prof);

        
        $__internal_4f467ed1db87d4bf155e4510290b7985554e811f327777545c19cd6fa4c297ee->leave($__internal_4f467ed1db87d4bf155e4510290b7985554e811f327777545c19cd6fa4c297ee_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_5f170627026a38cabea9a856835ffbf0bf189d95e2f27614fb38e0b9299ba3b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f170627026a38cabea9a856835ffbf0bf189d95e2f27614fb38e0b9299ba3b0->enter($__internal_5f170627026a38cabea9a856835ffbf0bf189d95e2f27614fb38e0b9299ba3b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_bce4babbd11ac8604de7259eca0ca7facd579cc464d99484629d0a3a7996ccda = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bce4babbd11ac8604de7259eca0ca7facd579cc464d99484629d0a3a7996ccda->enter($__internal_bce4babbd11ac8604de7259eca0ca7facd579cc464d99484629d0a3a7996ccda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_bce4babbd11ac8604de7259eca0ca7facd579cc464d99484629d0a3a7996ccda->leave($__internal_bce4babbd11ac8604de7259eca0ca7facd579cc464d99484629d0a3a7996ccda_prof);

        
        $__internal_5f170627026a38cabea9a856835ffbf0bf189d95e2f27614fb38e0b9299ba3b0->leave($__internal_5f170627026a38cabea9a856835ffbf0bf189d95e2f27614fb38e0b9299ba3b0_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b19dd34dfc7b026010510508e2e38eb896a646ed9b15d02a2e7b20a48f211134 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b19dd34dfc7b026010510508e2e38eb896a646ed9b15d02a2e7b20a48f211134->enter($__internal_b19dd34dfc7b026010510508e2e38eb896a646ed9b15d02a2e7b20a48f211134_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_c8014bc8ff67669e73380cc3acd8200809ed4badfe211a84a929d2991bcba51d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8014bc8ff67669e73380cc3acd8200809ed4badfe211a84a929d2991bcba51d->enter($__internal_c8014bc8ff67669e73380cc3acd8200809ed4badfe211a84a929d2991bcba51d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_c8014bc8ff67669e73380cc3acd8200809ed4badfe211a84a929d2991bcba51d->leave($__internal_c8014bc8ff67669e73380cc3acd8200809ed4badfe211a84a929d2991bcba51d_prof);

        
        $__internal_b19dd34dfc7b026010510508e2e38eb896a646ed9b15d02a2e7b20a48f211134->leave($__internal_b19dd34dfc7b026010510508e2e38eb896a646ed9b15d02a2e7b20a48f211134_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_ff69d33115474850750b09bf76703e5fb948892311fd2f77b3eb1a72777e9c86 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff69d33115474850750b09bf76703e5fb948892311fd2f77b3eb1a72777e9c86->enter($__internal_ff69d33115474850750b09bf76703e5fb948892311fd2f77b3eb1a72777e9c86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_871466a89af55752cc7ab0e1bc071b94aeca3d8b55be1a6340bf6c196508f187 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_871466a89af55752cc7ab0e1bc071b94aeca3d8b55be1a6340bf6c196508f187->enter($__internal_871466a89af55752cc7ab0e1bc071b94aeca3d8b55be1a6340bf6c196508f187_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_871466a89af55752cc7ab0e1bc071b94aeca3d8b55be1a6340bf6c196508f187->leave($__internal_871466a89af55752cc7ab0e1bc071b94aeca3d8b55be1a6340bf6c196508f187_prof);

        
        $__internal_ff69d33115474850750b09bf76703e5fb948892311fd2f77b3eb1a72777e9c86->leave($__internal_ff69d33115474850750b09bf76703e5fb948892311fd2f77b3eb1a72777e9c86_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ee60634c0587d7e32e018e5c7066c14ba3213e33e63fdc4a6c9eacfd930de5a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee60634c0587d7e32e018e5c7066c14ba3213e33e63fdc4a6c9eacfd930de5a8->enter($__internal_ee60634c0587d7e32e018e5c7066c14ba3213e33e63fdc4a6c9eacfd930de5a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_de1fbdf8fc6d6046e27bd1a689020a0431e384661f0ee7238c680d5cce04c095 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de1fbdf8fc6d6046e27bd1a689020a0431e384661f0ee7238c680d5cce04c095->enter($__internal_de1fbdf8fc6d6046e27bd1a689020a0431e384661f0ee7238c680d5cce04c095_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_de1fbdf8fc6d6046e27bd1a689020a0431e384661f0ee7238c680d5cce04c095->leave($__internal_de1fbdf8fc6d6046e27bd1a689020a0431e384661f0ee7238c680d5cce04c095_prof);

        
        $__internal_ee60634c0587d7e32e018e5c7066c14ba3213e33e63fdc4a6c9eacfd930de5a8->leave($__internal_ee60634c0587d7e32e018e5c7066c14ba3213e33e63fdc4a6c9eacfd930de5a8_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/ruslan/project/sum/app/Resources/views/base.html.twig");
    }
}
