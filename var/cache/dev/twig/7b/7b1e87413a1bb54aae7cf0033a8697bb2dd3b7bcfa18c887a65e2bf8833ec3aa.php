<?php

/* @Twig/Exception/exception.json.twig */
class __TwigTemplate_0f27f4fe9282c70ac30712e57138067493e9f3f1df6f346e10eb337b8c7c0d52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c94ed304eff43370f24e27b99ce0abfaeedd4ec2f99f9355999f7b14152a31d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c94ed304eff43370f24e27b99ce0abfaeedd4ec2f99f9355999f7b14152a31d3->enter($__internal_c94ed304eff43370f24e27b99ce0abfaeedd4ec2f99f9355999f7b14152a31d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        $__internal_be5b104b5313591e56e81e7bd7ffbeff413dd1b99648229c35dfadd756961f55 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be5b104b5313591e56e81e7bd7ffbeff413dd1b99648229c35dfadd756961f55->enter($__internal_be5b104b5313591e56e81e7bd7ffbeff413dd1b99648229c35dfadd756961f55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")), "exception" => $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_c94ed304eff43370f24e27b99ce0abfaeedd4ec2f99f9355999f7b14152a31d3->leave($__internal_c94ed304eff43370f24e27b99ce0abfaeedd4ec2f99f9355999f7b14152a31d3_prof);

        
        $__internal_be5b104b5313591e56e81e7bd7ffbeff413dd1b99648229c35dfadd756961f55->leave($__internal_be5b104b5313591e56e81e7bd7ffbeff413dd1b99648229c35dfadd756961f55_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "@Twig/Exception/exception.json.twig", "/home/ruslan/project/sum/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
